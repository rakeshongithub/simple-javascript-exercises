modulas
=======

#### 1. Modulas example with table row color

```javascript

var colors = ['#999999','#000000','#cccccc'],
    myTrs = document.getElementsByTagName('tr');
	
for(i=0, len = myTrs.length; i < len; i++){
  myTrs[i].style.backgroundColor = colors[i % colors.length]
}
```

#### 2. Hack to mimic console within JSFiddle

```javascript
var consoleLine = "<div class=\"console-line\"></div>";
console = {
    log: function (text) {
        $("#console-log").append($(consoleLine).html(text));
    }
};
```

#### 3. Write a function translate() that will translate a text into "rövarspråket". That is, double every consonant and place an occurrence of "o" in between. For example, translate("this is fun") should return the string "tothohisos isos fofunon".

```javascript
function isVowel(param,o){
	var char = param.split("");
	for(var i = 0; i<char.length; i++){
	var newChar, charArr = char[i];

		if( !(/[aeiou]/i.test(charArr)) ) {
			if(!(/\s/.test(charArr))){
				 newChar = charArr + o + charArr;
			} else {
				 newChar = charArr;
			}
		} else{ newChar = charArr;}
		
		// Joining all splited text with code.
		document.getElementById("text").innerHTML += newChar;
		
	};
};

isVowel("This is fun",'o');
```

#### 4. Define a function sum() and a function multiply() that sums and multiplies (respectively) all the numbers in an array of numbers. For example, sum([1,2,3,4]) should return 10, and multiply([1,2,3,4]) should return 24.

```javascript

function mathmetics(logic, param){
	var i, result;
	if(logic == "sum"){
		result = 0;
		for(i=0; i < param.length; i++){
			result += param[i];
		}
		alert("Sum Result is: "+result);
	} else if(logic == "multiply"){
		result = 1;
		for(i=0; i < param.length; i++){
			result *= param[i];
		}
		alert("Multiply Result is: "+result);
	}
};
mathmetics("multiply", [1,2,3,4]);

```

#### 5. Define a function reverse() that computes the reversal of a string. For example, reverse("jag testar") should return the string "ratset gaj".

```javascript
function reverse(param){
	var i, strArr, newStr="";

	strArr = param.split("");
	len = strArr.length-1;

	for(i=len; i>=0; i--){
		newStr += strArr[i];
	}
	document.getElementById("text").innerHTML = newStr;
};

reverse("jag testar");
```